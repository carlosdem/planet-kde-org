#
msgid ""
msgstr ""
"Project-Id-Version: websites-planet-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-29 00:45+0000\n"
"PO-Revision-Date: 2022-07-22 16:46+0200\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"SPDX-FileCopyrightText: FULL NAME <EMAIL@ADDRESS>\n"
"SPDX-License-Identifier: LGPL-3.0-or-later\n"
"X-Generator: Poedit 3.1.1\n"

#: config.yaml:0
msgid "Planet KDE"
msgstr "Planet KDE"

#: config.yaml:0
msgid "Planet KDE site providing newest news from the KDE Project"
msgstr ""
"Planet KDE წარმოადგენს ვებსაიტს, საიდაც KDE -ს შესახებ უახლესი სიახლეების "
"მიღება შეგიძლიათ"

#: config.yaml:0
msgid "Add your own feed"
msgstr "დაამატეთ თქვენი პირადი ნაკადი"

#: i18n/en.yaml:0
msgid "Welcome to Planet KDE"
msgstr "კეთილი იყოს თქვენი მობრძანება Planet KDE-ზე"

#: i18n/en.yaml:0
msgid ""
"This is a feed aggregator that collects what the contributors to the [KDE "
"community](https://kde.org) are writing on their respective blogs, in "
"different languages"
msgstr ""
"ეს ნაკადის შემკრებია, რომელიც მოიცავს, რას წერენ KDE-ს შემომწირველები "
"(https://kde.org) თავიანთ ბლოგებზე, სხვადასხვა ენაზე"

#: i18n/en.yaml:0
msgid "Go down one post"
msgstr "ერთი პოსტით დაბლა ჩასვლა"

#: i18n/en.yaml:0
msgid "Go up one post"
msgstr "ერთი პოსტით მაღლა ასვლა"
