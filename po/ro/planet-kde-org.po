#
# Sergiu Bivol <sergiu@cip.md>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: websites-planet-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-29 00:45+0000\n"
"PO-Revision-Date: 2022-02-06 17:17+0000\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.2\n"

#: config.yaml:0
msgid "Planet KDE"
msgstr "Planeta KDE"

#: config.yaml:0
msgid "Planet KDE site providing newest news from the KDE Project"
msgstr ""
"Saitul „Planeta KDE” oferă cele mai proaspete știri de la proiectul KDE"

#: config.yaml:0
msgid "Add your own feed"
msgstr "Adaugă propriul flux"

#: i18n/en.yaml:0
msgid "Welcome to Planet KDE"
msgstr "Bun venit pe Planeta KDE"

#: i18n/en.yaml:0
msgid ""
"This is a feed aggregator that collects what the contributors to the [KDE "
"community](https://kde.org) are writing on their respective blogs, in "
"different languages"
msgstr ""
"Acesta este un agregator de fluxuri care adună cele scrise de contribuitorii "
"la [Comunitatea KDE](https://kde.org) pe blogurile lor, în diferite limbi"

#: i18n/en.yaml:0
msgid "Go down one post"
msgstr "Mergi cu un articol în jos"

#: i18n/en.yaml:0
msgid "Go up one post"
msgstr "Mergi cu un articol în sus"
